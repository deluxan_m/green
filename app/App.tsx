import React from 'react';
import {View, StyleSheet, StatusBar} from 'react-native';
import {COLORS} from './constants';

import {BottomNav} from './navigation';

const App = () => {
  return (
    <>
      <StatusBar backgroundColor={COLORS.GREEN} />
      <BottomNav />
    </>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default App;
