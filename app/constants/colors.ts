interface COLORSI {
  GREEN: string;
  ORANGE: string;
  BLACK: string;
  GREY_L: string;
  GREY_M: string;
  GREY_D: string;
}

export const COLORS: COLORSI = {
  GREEN: '#53B175',
  ORANGE: '#F3603F',
  BLACK: '#181725',
  GREY_L: '#DFDFDF',
  GREY_M: '#DCDCDC',
  GREY_D: '#AAAAAA',
};
