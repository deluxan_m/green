import {COLORS} from './colors';
import {ICONS} from './icons';
import {SIZES} from './sizes';
import {IMAGES} from './images';
import {PRODUCTS} from './products';

export {COLORS, ICONS, SIZES, IMAGES, PRODUCTS};
