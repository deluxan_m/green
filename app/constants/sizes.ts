import {Dimensions} from 'react-native';

interface SCREENI {
  WIDTH: number;
  HEIGHT: number;
}

interface SHAPEI {
  RADIUS: number;
  PADDING: number;
  MARGIN: number;
}

interface FONTSI {
  SMALL: number;
  MEDIUM: number;
  LARGE: number;
  HUGE: number;
}

interface SIZESI {
  SCREEN: SCREENI;
  SHAPE: SHAPEI;
  FONTS: FONTSI;
}

export const SIZES: SIZESI = {
  SCREEN: {
    WIDTH: Dimensions.get('window').width,
    HEIGHT: Dimensions.get('window').height,
  },
  SHAPE: {
    RADIUS: 5,
    PADDING: 5,
    MARGIN: 5,
  },
  FONTS: {
    SMALL: 14,
    MEDIUM: 20,
    LARGE: 24,
    HUGE: 28,
  },
};
