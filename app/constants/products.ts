import product1 from '../assets/images/products/product1.png';
import product2 from '../assets/images/products/product2.png';

export interface PRODUCTSI {
  id: string;
  image: any;
  name: string;
  quantity: number;
  unit: string;
  price: number;
}

export const PRODUCTS: PRODUCTSI[] = [
  {
    id: 'product_1',
    image: product1,
    name: 'Banana',
    quantity: 7,
    unit: 'pcs',
    price: 4.99,
  },
  {
    id: 'product_2',
    image: product2,
    name: 'Apple',
    quantity: 1,
    unit: 'pcs',
    price: 2.99,
  },
  {
    id: 'product_3',
    image: product2,
    name: 'Orange',
    quantity: 5,
    unit: 'pcs',
    price: 8.99,
  },
  {
    id: 'product_4',
    image: product1,
    name: 'Lemon',
    quantity: 10,
    unit: 'pcs',
    price: 9.99,
  },
  {
    id: 'product_5',
    image: product2,
    name: 'Rice',
    quantity: 1,
    unit: 'kg',
    price: 8.99,
  },
];
