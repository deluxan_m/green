import banner from '../assets/images/banner.png';
import banner1 from '../assets/images/banner-template.jpg';

export const IMAGES = {
  BANNER: [
    {
      id: 'banner1',
      image: banner,
    },
    {
      id: 'banner2',
      image: banner1,
    },
    {
      id: 'banner3',
      image: banner,
    },
    {
      id: 'banner4',
      image: banner1,
    },
  ],
};
