import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Space} from '../atoms';
import {IconText, IconInput} from '../molecules';

interface Iprops {
  icon: string;
  variant: string;
  text: string;
  searchHint: string;
  searchValue: (text: string) => string;
}

const SearchWithTitle = (props: Iprops) => {
  const {icon, variant, text, searchHint, searchValue} = props;

  const handleValue = (_text: string) => searchValue(_text);

  return (
    <View style={styles.container}>
      <IconText icon={icon} variant={variant} text={text} />
      <Space space={2} />
      <IconInput hint={searchHint} retrieveValue={handleValue} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
});

export default SearchWithTitle;
