import React from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';

import {PRODUCTSI} from '../../constants/products';
import {SIZES} from '../../constants';

import {Space} from '../atoms';
import {TitleWithLink, ProductCard} from '../molecules';

interface IProps {
  title: string;
  link: string;
  handlePress: () => void;
  data: PRODUCTSI[];
}

const HorizontalList = (props: IProps) => {
  const {title, link, handlePress, data} = props;

  const handleItem = (item: any) => {
    console.log(`opening ${item.name}`);
  };

  const renderItem = ({item}: any) => {
    return (
      <View>
        <ProductCard
          image={item.image}
          name={item.name}
          quantity={item.quantity}
          unit={item.unit}
          price={item.price}
          handlePress={() => handleItem(item)}
        />
      </View>
    );
  };

  return (
    <>
      <TitleWithLink title={title} link={link} handlePress={handlePress} />
      <Space space={2} />
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal
        style={styles.list}
        showsHorizontalScrollIndicator={false}
      />
    </>
  );
};

const styles = StyleSheet.create({
  list: {
    // marginHorizontal: SIZES.SHAPE.MARGIN * 4,
  },
});

export default HorizontalList;
