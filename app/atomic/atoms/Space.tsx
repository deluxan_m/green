import React from 'react';
import {View} from 'react-native';

import {SIZES} from '../../constants';

interface IProps {
  space: number;
}

const Space = (props: IProps) => {
  return <View style={{marginVertical: SIZES.SHAPE.MARGIN * props.space}} />;
};

export default Space;
