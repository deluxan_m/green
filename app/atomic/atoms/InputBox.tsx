import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';

import {COLORS, SIZES} from '../../constants';

interface IProps {
  hint: string;
  style: Object;
  getData: (text: string) => void;
}

const InputBox = (props: IProps) => {
  const handleChange = (text: string) => props.getData(text);

  return (
    <View style={styles.container}>
      <TextInput
        style={[styles.input, props.style]}
        placeholder={props.hint}
        onChangeText={handleChange}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  input: {
    backgroundColor: COLORS.GREY_L,
    width: SIZES.SCREEN.WIDTH * 0.85,
    borderRadius: SIZES.SHAPE.RADIUS * 2,
    color: COLORS.GREY_D,
  },
});

export default InputBox;
