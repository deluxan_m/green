import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants';

interface IProps {
  text: string;
  handlePress: () => void;
}

const TextButton = (props: IProps) => {
  const {text, handlePress} = props;

  return (
    <TouchableOpacity style={styles.container} onPress={handlePress}>
      <Text style={styles.title}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {},
  title: {
    color: COLORS.GREEN,
    fontSize: SIZES.FONTS.MEDIUM,
    fontWeight: 'bold',
  },
});

export default TextButton;
