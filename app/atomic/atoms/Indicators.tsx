import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {COLORS} from '../../constants';

interface IProps {
  length: number;
  active: number;
}

const Indicators = (props: IProps) => {
  const {length, active} = props;

  const indicators = () => {
    return [...Array(length)].map((item: number, index: number) => {
      return (
        <View
          key={index}
          style={[
            styles.item,
            // eslint-disable-next-line react-native/no-inline-styles
            {
              width: index === active ? 15 : 5,
              backgroundColor: index === active ? COLORS.GREEN : COLORS.GREY_D,
            },
          ]}
        />
      );
    });
  };

  return <View style={styles.container}>{indicators()}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  item: {
    marginRight: 2,
    height: 5,
    borderRadius: 5,
  },
});

export default Indicators;
