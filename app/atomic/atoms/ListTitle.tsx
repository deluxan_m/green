import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants';

interface IProps {
  title: string;
}

const ListTitle = (props: IProps) => {
  const {title} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  title: {
    color: COLORS.BLACK,
    fontSize: SIZES.FONTS.HUGE,
  },
});

export default ListTitle;
