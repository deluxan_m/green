import React from 'react';
import {View, StyleSheet} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Zocial from 'react-native-vector-icons/Zocial';

import {ICONS} from '../../constants';

interface IProps {
  name: string;
  variant: string;
  size: number;
  color: string;
  style?: Object;
}

const Icons = (props: IProps) => {
  const {name, variant, size, color, style} = props;

  const renderIcon = () => {
    switch (variant) {
      case ICONS.AntDesign:
        return (
          <AntDesign name={name} color={color} size={size} style={style} />
        );
      case ICONS.Entypo:
        return <Entypo name={name} color={color} size={size} style={style} />;
      case ICONS.EvilIcons:
        return (
          <EvilIcons name={name} color={color} size={size} style={style} />
        );
      case ICONS.Feather:
        return <Feather name={name} color={color} size={size} style={style} />;
      case ICONS.FontAwesome:
        return (
          <FontAwesome name={name} color={color} size={size} style={style} />
        );
      case ICONS.FontAwesome5:
        return (
          <FontAwesome5 name={name} color={color} size={size} style={style} />
        );
      case ICONS.Fontisto:
        return <Fontisto name={name} color={color} size={size} style={style} />;
      case ICONS.Foundation:
        return (
          <Foundation name={name} color={color} size={size} style={style} />
        );
      case ICONS.Ionicons:
        return <Ionicons name={name} color={color} size={size} style={style} />;
      case ICONS.MaterialCommunity:
        return (
          <MaterialCommunity
            name={name}
            color={color}
            size={size}
            style={style}
          />
        );
      case ICONS.MaterialIcons:
        return (
          <MaterialIcons name={name} color={color} size={size} style={style} />
        );
      case ICONS.Octicons:
        return <Octicons name={name} color={color} size={size} style={style} />;
      case ICONS.Zocial:
        return <Zocial name={name} color={color} size={size} style={style} />;
      default:
        return (
          <FontAwesome name={name} color={color} size={size} style={style} />
        );
    }
  };

  return <View>{renderIcon()}</View>;
};

export default Icons;
