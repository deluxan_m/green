import React from 'react';
import {View, StyleSheet} from 'react-native';

import {Icons, InputBox} from '../atoms';
import {ICONS, SIZES, COLORS} from '../../constants';

interface IProps {
  hint: string;
  retrieveValue: (text: string) => void;
}

const IconInput = (props: IProps) => {
  const handleValue = (text: string) => {
    props.retrieveValue(text);
  };

  return (
    <View style={styles.container}>
      <Icons
        variant={ICONS.FontAwesome}
        name="search"
        size={18}
        color={COLORS.GREY_D}
        style={styles.icon}
      />
      <InputBox
        hint={props.hint}
        style={styles.input}
        getData={(text: string) => handleValue(text)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.GREY_L,
    borderRadius: SIZES.SHAPE.RADIUS * 2,
  },
  icon: {
    marginLeft: SIZES.SHAPE.PADDING,
  },
  input: {},
});

export default IconInput;
