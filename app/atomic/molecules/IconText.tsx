import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Icons} from '../atoms';
import {ICONS} from '../../constants';

interface IProps {
  icon: string;
  variant: string;
  text: string;
}

const IconText = (props: IProps) => {
  return (
    <View style={styles.container}>
      <Icons variant={props.variant} name={props.icon} size={18} color="#222" />
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 5,
    fontSize: 16,
  },
});

export default IconText;
