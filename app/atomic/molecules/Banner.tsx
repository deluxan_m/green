import React, {useState, useEffect} from 'react';
import {View, Image, StyleSheet, Animated} from 'react-native';

import {IMAGES, SIZES} from '../../constants';
import {Indicators, Space} from '../atoms';

const Banner = () => {
  const [visible, setVisible] = useState(0);
  const [fadeValue, setFadeValue] = useState(new Animated.Value(0));

  const fadeAnim = () => {
    Animated.timing(fadeValue, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    fadeAnim();

    setInterval(() => {
      if (visible === IMAGES.BANNER.length - 1) {
        setVisible(0);
      } else {
        setVisible(visible + 1);
      }
    }, 5000);
  });

  const banner = () => {
    return IMAGES.BANNER.map((image: any, index: number) => {
      return (
        index === visible && (
          <Animated.View key={image.id} style={{opacity: fadeValue}}>
            <Image
              source={image.image}
              style={styles.image}
              height={120}
              width={SIZES.SCREEN.WIDTH * 0.9}
            />
          </Animated.View>
        )
      );
    });
  };

  return (
    <View style={styles.container}>
      {banner()}
      <Space space={1} />
      <Indicators length={IMAGES.BANNER.length} active={visible} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {},
});

export default Banner;
