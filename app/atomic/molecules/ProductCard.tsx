import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

import {COLORS, SIZES} from '../../constants';

import {Space} from '../atoms';

interface IProps {
  image: any;
  name: string;
  quantity: number;
  unit: string;
  price: number;
  handlePress: (item: any) => void;
}

const ProductCard = (props: IProps) => {
  const {image, name, quantity, unit, price, handlePress} = props;

  return (
    <TouchableOpacity style={styles.container} onPress={handlePress}>
      <Image source={image} />
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.quantity}>{`${quantity}${unit}`}</Text>
      <Space space={2} />
      <View>
        <Text style={styles.price}>${price}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.2,
    marginRight: SIZES.SHAPE.MARGIN * 4,
    borderRadius: SIZES.SHAPE.RADIUS * 2,
    padding: SIZES.SHAPE.PADDING * 4,
  },
  name: {
    fontSize: SIZES.FONTS.LARGE,
    fontWeight: '700',
    marginTop: SIZES.SHAPE.MARGIN * 2,
  },
  quantity: {
    color: COLORS.GREY_D,
    fontSize: SIZES.FONTS.SMALL,
  },
  price: {
    fontSize: SIZES.FONTS.HUGE,
    fontWeight: 'bold',
  },
});

export default ProductCard;
