import React from 'react';
import {View, StyleSheet} from 'react-native';

import {SIZES} from '../../constants';

import {ListTitle, TextButton} from '../atoms';

interface IProps {
  title: string;
  link: string;
  handlePress: () => void;
}

const TitleWithLink = (props: IProps) => {
  const {title, link, handlePress} = props;

  return (
    <View style={styles.container}>
      <ListTitle title={title} />
      <TextButton text={link} handlePress={handlePress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // width: SIZES.SCREEN.WIDTH * 0.9,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default TitleWithLink;
