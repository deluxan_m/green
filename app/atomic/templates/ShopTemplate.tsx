import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

import {SIZES} from '../../constants';
import {PRODUCTSI} from '../../constants/products';

import {Space} from '../atoms';
import {TitleWithLink} from '../molecules';
import {SearchWithTitle, HorizontalList} from '../organisms';

interface IProps {
  icon: string;
  variant: string;
  text: string;
  searchHint: string;
  searchValue: (text: string) => string;
  title1: string;
  link1: string;
  handlePress1: () => void;
  data1: PRODUCTSI[];
  title2: string;
  link2: string;
  handlePress2: () => void;
  data2: PRODUCTSI[];
  title3: string;
  link3: string;
  handlePress3: () => void;
  data3: PRODUCTSI[];
}

const ShopTemplate = (props: IProps) => {
  const {
    icon,
    variant,
    text,
    searchHint,
    searchValue,
    title1,
    link1,
    handlePress1,
    data1,
    title2,
    link2,
    handlePress2,
    data2,
    title3,
    link3,
    handlePress3,
    data3,
  } = props;

  return (
    <View style={styles.container}>
      <SearchWithTitle
        icon={icon}
        variant={variant}
        text={text}
        searchHint={searchHint}
        searchValue={searchValue}
      />
      <Space space={2} />
      <ScrollView style={styles.scroll}>
        <HorizontalList
          title={title1}
          link={link1}
          handlePress={handlePress1}
          data={data1}
        />
        <Space space={2} />
        <HorizontalList
          title={title2}
          link={link2}
          handlePress={handlePress2}
          data={data2}
        />
        <Space space={2} />
        <HorizontalList
          title={title3}
          link={link3}
          handlePress={handlePress3}
          data={data3}
        />
        <Space space={10} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: SIZES.SHAPE.MARGIN * 4,
  },
  scroll: {
    marginHorizontal: SIZES.SHAPE.MARGIN * 4,
  },
});

export default ShopTemplate;
