import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Favourite = (props: any) => {
  return (
    <View style={styles.container}>
      <Text>Favourite</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Favourite;
