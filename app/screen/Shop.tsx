import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {ShopTemplate} from '../atomic/templates';

import {ICONS, PRODUCTS} from '../constants';

const Shop = (props: any) => {
  const [searchText, setSearchText] = useState('');

  const seeAllExclusive = () => console.log('seeAllExclusive');

  const handleValue = (text: string) => {
    console.log(text);
    setSearchText(text);
    return text;
  };

  return (
    <ShopTemplate
      icon="map-marker"
      variant={ICONS.FontAwesome}
      text="Palaikuli, Mannar"
      searchHint="Search"
      searchValue={(text: string) => handleValue(text)}
      title1="Exclusive offer"
      link1="See all"
      handlePress1={seeAllExclusive}
      data1={PRODUCTS}
      title2="Best selling"
      link2="See all"
      handlePress2={seeAllExclusive}
      data2={PRODUCTS}
      title3="Best selling"
      link3="See all"
      handlePress3={seeAllExclusive}
      data3={PRODUCTS}
    />
  );
};

const styles = StyleSheet.create({});

export default Shop;
