import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Shop, Explore} from '../screen';

const Stack = createStackNavigator();

export function StackNav() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Shop} />
      <Stack.Screen name="Settings" component={Explore} />
    </Stack.Navigator>
  );
}
