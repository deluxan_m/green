import * as React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MAIcon from 'react-native-vector-icons/MaterialIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';

import {Shop, Explore, Cart, Favourite, Account} from '../screen';
import {COLORS} from '../constants';

const Tab = createBottomTabNavigator();

export function BottomNav() {
  const {GREEN, BLACK} = COLORS;

  function menuSingle(route: any, tabBarIcon: any, icon: string) {
    const {color, size, focused} = tabBarIcon;

    function renderIcon() {
      switch (route.name) {
        case 'Shop':
          return <EntypoIcon name={icon} color={color} size={size} />;
        case 'Explore':
          return <MCIcon name={icon} color={color} size={size} />;
        case 'Cart':
          return <MCIcon name={icon} color={color} size={size} />;
        case 'Favourite':
          return <Ionicon name={icon} color={color} size={size} />;
        case 'Account':
          return <MAIcon name={icon} color={color} size={size} />;
        default:
          return <FAIcon name={icon} color={color} size={size} />;
      }
    }

    return (
      <View style={styles.menu}>
        {renderIcon()}
        <Text style={[styles.menuName, {color: focused ? GREEN : BLACK}]}>
          {route.name}
        </Text>
      </View>
    );
  }

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: GREEN,
        inactiveTintColor: BLACK,
        style: {
          elevation: 20,
          height: 60,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
          backgroundColor: '#fff',
          position: 'absolute',
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: (tabBarIcon: any) => {
          if (route.name === 'Shop') {
            const icon = 'shop';
            return menuSingle(route, tabBarIcon, icon);
          } else if (route.name === 'Explore') {
            const icon = 'text-box-search-outline';
            return menuSingle(route, tabBarIcon, icon);
          } else if (route.name === 'Cart') {
            const icon = 'cart-outline';
            return menuSingle(route, tabBarIcon, icon);
          } else if (route.name === 'Favourite') {
            const icon = 'heart-outline';
            return menuSingle(route, tabBarIcon, icon);
          } else if (route.name === 'Account') {
            const icon = 'person-outline';
            return menuSingle(route, tabBarIcon, icon);
          }
        },
      })}>
      <Tab.Screen name="Shop" component={Shop} />
      <Tab.Screen name="Explore" component={Explore} />
      <Tab.Screen name="Cart" component={Cart} />
      <Tab.Screen name="Favourite" component={Favourite} />
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  menu: {
    alignItems: 'center',
  },
  menuName: {
    fontSize: 12,
    fontWeight: '700',
  },
});
