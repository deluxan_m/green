interface Action {
  type: string;
  payload: any;
}

const initial_state = {};

const authReducer = (state = initial_state, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default authReducer;
