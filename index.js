import 'react-native-gesture-handler';
import * as React from 'react';
import {AppRegistry} from 'react-native';
import App from './app/App';
import {name as appName} from './app/app.json';

import {NavigationContainer} from '@react-navigation/native';

import {Provider} from 'react-redux';
import store from './app/redux/store';

const Root = () => (
  <NavigationContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </NavigationContainer>
);

AppRegistry.registerComponent(appName, () => Root);
